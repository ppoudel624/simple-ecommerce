import React  from 'react';
import { Card,Button } from 'react-bootstrap';
import ProductDetails from './ProductDetails';
import {Link} from 'react-router-dom';
import {addBasket} from '../action/Add';
import {connect} from 'react-redux';



 function Product(props){
   
     return(
         
         
<div style={{paddingTop:'8%' }}>
 
   
<Card key={props.value.id} style={{ width: '18rem' }}>
<Link to={'/products/'+props.value._id}>
  <Card.Img variant="top" src={props.value.image} />
  </Link>

  <Card.Body>
     <Card.Title>{props.value.title}</Card.Title>
    <Card.Text>
      Some quick example text to build on the card title and make up the bulk of
      the card's content.
    </Card.Text>
  

 <Button  onClick={()=>props.addBasket(props.value._id) } variant="primary"> Add to cart</Button>


 
  </Card.Body>
</Card>


</div>
     )
 }
 
const mapStateToProps=state=>({
  basketProps:state.basketState
})
export default connect(null,{addBasket}) (Product);
