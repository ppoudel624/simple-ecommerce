import React, {useState, Component} from 'react';
import data from '../Data/Data';
import { Container,Col,Row ,Button} from 'react-bootstrap';
import { AiOutlineStar, AiFillStar } from 'react-icons/ai';
import Product from'./Product';
import '../App.css';
import Related from './Related';
import { render } from '@testing-library/react';

function ProductDetails(props){
  console.log(props)
    // console.log(props.match.params.id);
    const product=data.products.find(x=>x._id === props.match.params.id);
  
     return(
      <div>
        <Row>
    <Col>
    
    <img className="photo"  src={product.image} alt=""/>
    
    </Col>
     <Col>
    <h1>{product.title}</h1>
    <AiFillStar color="#f3650a" />
      <AiFillStar color="#f3650a" />
       <AiFillStar color="#f3650a" />
        <AiOutlineStar color="#f3650a" />
      <AiOutlineStar color="#f3650a" />
     <p>{product.price}</p>
     <p>{product.description}</p>
     
     </Col>
     
  </Row>


  </div> 
   
   
    )
   
   
}

export default ProductDetails;