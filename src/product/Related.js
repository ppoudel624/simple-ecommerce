import React  from 'react';
import { Card,Button, Container } from 'react-bootstrap';
import {addBasket} from '../action/Add';


 function Related(props){
     return(
      
         <div style={{paddingTop:'8%' }}>
 
   <Container>
<Card key={props.value.id} style={{ width: '18rem' }}>

  <Card.Img variant="top" src={props.value.image} />
  

  <Card.Body>
     <Card.Title>{props.value.title}</Card.Title>
    <Card.Text>
      Some quick example text to build on the card title and make up the bulk of
      the card's content.
    </Card.Text>
  

 <Button onClick={()=>addBasket(props.value._id)}  variant="primary"> Add to cart</Button>


 
  </Card.Body>
</Card>
</Container>

</div>
     )
 }
 export default Related;


 