import image1 from '../Images/image-1.jpg';
import image2 from '../Images/image-2.jpg';
import image3 from '../Images/image-3.jpg';
import image4 from '../Images/image-4.jpg';
import image5 from '../Images/pants.jpg';
import image6 from '../Images/shirt1.jpg';
import image7 from '../Images/shoes.png';
import image8 from '../Images/jacket1.jpg';


export default {
    
        products:[
            {

            _id: '1',
            title: "Collection Sneaker 2020",
            subtitle: "Nike Free RN 2020 Men's Running Shoe ",
            description: "Discount 10% for all customers booked before 02/14/2020",
            image:image1,
            price:'Rs.200',
        },
        {

        _id: '2',
        title: "Collection Clothing 2020",
        subtitle: "Best Price Ski Jacket For Men 2018",
        description: "Light weight warm-up softshell jacket combining wind-resistance and breathability. Ideal for active nordic skiing or winter training.",
        price:'Rs.200',
        image: image2
        },

        {
            _id: '3',
        title: "Collection Camera 2020",
        subtitle: "Best Price Ski Jacket For Men 2018",
        description: "Light weight warm-up softshell jacket combining wind-resistance and breathability. Ideal for active nordic skiing or winter training.",
        image: image3,
        price:'Rs.200',
        },

    
    {
        _id: 4,
        title: "Collection laptop 2020",
        subtitle: "Best Price Ski Jacket For Men 2018",
        description: "Light weight warm-up softshell jacket combining wind-resistance and breathability. Ideal for active nordic skiing or winter training.",
       image:image4,
       price:'Rs.200',
    },
],

relatedproducts:[

    {
        id: 1,
        title: "Vans Black VN00018CJPX Authentic Decon Shoes For Uniesx- 6202",
        category: "shoes",
        price: "2012",
        ud_price: "201",
        description: "Converse’s range of sneakers need no introduction as they are a global brand with almost cult-like following. Steeped in history, unique in design and reliable in function, Converse has never failed anyone.",
        image:image5
       
    },

    {
        id: 2,
        title: "Jackets For Men",
        category: "jackets",
        price: "230",
        ud_price: "205",
        description: "Autumn Windproof Street Wear Jacket,Hoodie Pocket Sport Outerwear Windbreaker ",
        image:image6
    },

    {
        id: 3,
        title: "Green Cotton Tshirt For Men - Plain T-Shirt",
        category: "shirt",
        price: "120",
        ud_price: "200",
        description: "This is best quality t-shirt designed for all fitness-conscious men, This T-shirt is light in weight and comfortable to wear, courtesy the 100 % cotton fabric.",
        image:image7
    },

    {
        id: 4,
        title: "Mollit reprehenderit",
        category: "shoes",
        price: "222",
        ud_price: "12",
        description: "1Do ex mollit tempor amet excepteur.Nulla dolor aute ad dolore adipisicing ullamco aliqua occaecat proident excepteur non minim velit sint.",
        image:image8
    },   
]
}
