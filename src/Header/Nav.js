import React, { useState } from 'react';
import Navigation from './Navigation';
import Slider from './Slider';
import { Navbar } from 'react-bootstrap';
import data from '../Data/Data'
import Product from '../product/Product';
import Related from '../product/Related'






function Nav(){
  // const[data]=useState(data);

  const design={
  display:'flex',
  justifyContent:'space-around'
  }
         return(
            
              <div>
  <Navbar bg="dark">
    <Navbar.Brand href="#home">
    </Navbar.Brand>
  </Navbar>
  
  <Slider/>
  <div style={{marginTop:'45px'}}>
 <h1> Featured Products</h1>
 </div>
  
  
<div style={design}>
  {
  data.products.map(item=>
    <Product value={item}/>
    )
   
}
</div> 
<div style={{marginTop:'45px'}}>
 <h1> Related Products</h1>
 </div>
<div style={design}>
  
{
data.relatedproducts.map(related=>
  <Related value={related}/>
         )
}
</div>



  </div>
      
        )
        
    
}
export default Nav;