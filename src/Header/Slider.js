import React from 'react';
import { Carousel } from 'react-bootstrap';
import slider1 from '../Images/slider-1.jpg';
import slider2 from '../Images/slider-2.jpg';
import slider3 from '../Images/slider-3.jpg';
function Slider(){

return(


<Carousel>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src={slider1}
     
    />
    <Carousel.Caption>
      
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src={slider2}
     
    />

    <Carousel.Caption>
     
    
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src={slider3}
     
    />

    <Carousel.Caption>
    
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>
)
}
export default Slider;