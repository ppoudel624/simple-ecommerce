import React, { useEffect }  from 'react';
import { Navbar,Form,FormControl,Button,NavDropdown,Nav } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {getNumbers} from '../action/getAction';




function Navigation(props){
  useEffect(()=>{
    getNumbers();
  },[])
    return(
        <div>
        
            <Navbar bg="light" expand="lg">
  
  <Link to='/'><Navbar.Brand href="#home">Shopify
  </Navbar.Brand>
  </Link>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
     {/* <Link to='/'>
      <Nav.Link href="#home">Home</Nav.Link>
      </Link> */}
      <Link to='/shop'>
      <Nav.Link href="#link">Shop</Nav.Link>
      </Link>
      <NavDropdown title="Dropdown" id="basic-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
      </NavDropdown>
   <Link to='/cart'>
    <Button variant="warning">cart <span>{props.basketProps.basketNumbers}</span></Button>{' '}
    </Link>
    </Nav>
    <Form inline>

      <FormControl type="text" placeholder="Search" className="mr-sm-2" />
      <Button variant="outline-success">Search</Button>
    </Form>
  </Navbar.Collapse>
</Navbar>

        </div>
    )
}
const mapStateToProps=state=>({
  basketProps:state.basketState
})

export default connect(mapStateToProps,{getNumbers}) (Navigation);