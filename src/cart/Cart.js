
import React, { Component } from 'react';
import {connect} from 'react-redux';
import {productQuantity} from '../action/productQuantity';
import { Card, ListGroup,ListGroupItem,Button, Container } from 'react-bootstrap';
import image1 from '../Images/image-1.jpg';

function Cart({basketProps,productQuantity}){
 
  let productItem=[]
    Object.keys(basketProps.products).forEach(

        function(item){
      // console.log(item)
      
      if(basketProps.products[item].inCart){
      productItem.push(basketProps.products[item])
      }
     
    }    
    )
   
    productItem=productItem.map((product)=>{
     
      return(
        <Container >
        <Card style={{ width: '18rem' }}>
  <Card.Img variant="top" src={product.image} />
  <Card.Body>
      <Card.Title>{product.title}</Card.Title>
    
  </Card.Body>
  <ListGroup style={{fontSize:'16px'}} className="list-group-flush">
      <ListGroupItem>
      
        price:{product.price}
      
        </ListGroupItem>

      <ListGroupItem>
      <Button onClick={()=>productQuantity('increase',product._id)}  variant="warning">+</Button>{' '}
        Qty:{product.numbers}
        <Button onClick={()=>productQuantity('decrease',product._id)} variant="danger">-</Button>
        </ListGroupItem>
      <ListGroupItem>Total:{basketProps.cartCost}</ListGroupItem>
  </ListGroup>
  <Card.Body>
 
 
  </Card.Body>
</Card>
</Container>
      )
    })
    if(productItem!=''){
      return(
        <div>
          <h1>
            {productItem}
            </h1>
      </div>
      
      )
    }
    else{
      return(
        <h1>No item</h1>
      )
    }
}

      
const mapStateToProps=state=>({
    basketProps:state.basketState
  })

  

export default connect(mapStateToProps,{productQuantity}) (Cart);