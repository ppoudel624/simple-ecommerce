import {ADD_PRODUCT_BASKET, INCREASE_QUANTITY,DECREASE_QUANTITY} from '../action/types';
import {GET_NUMBERS_BASKET} from '../action/types';

import image1 from '../Images/image-1.jpg';
import image2 from '../Images/image-2.jpg';
import image3 from '../Images/image-3.jpg';
import image4 from '../Images/image-4.jpg';
const intialState={
    basketNumbers:0,
    cartCost:0,
    products:{
        1:{
           _id: '1',
        title: "Collection Sneaker 2020",
        subtitle: "Nike Free RN 2020 Men's Running Shoe ",
        description: "Discount 10% for all customers booked before 02/14/2020",
        image:image1,
        price:200  ,
        numbers:0,
        inCart:false
        },

       2: {

            _id: '2',
            title: "Collection Clothing 2020",
            subtitle: "Best Price Ski Jacket For Men 2018",
            description: "Light weight warm-up softshell jacket combining wind-resistance and breathability. Ideal for active nordic skiing or winter training.",
            price:300,
            numbers:0,
            image: image2,
            inCart:false,
            numbers:0,
      
            },

        3: {
                _id: '3',
            title: "Collection Camera 2020",
            subtitle: "Best Price Ski Jacket For Men 2018",
            description: "Light weight warm-up softshell jacket combining wind-resistance and breathability. Ideal for active nordic skiing or winter training.",
            image: image3,
            inCart:false,
            numbers:0,
            price:400,
            },

         4:  {
                _id: 4,
                title: "Collection laptop 2020",
                subtitle: "Best Price Ski Jacket For Men 2018",
                description: "Light weight warm-up softshell jacket combining wind-resistance and breathability. Ideal for active nordic skiing or winter training.",
               image:image4,
               inCart:false,
               numbers:0,
               price:500,
            }


              
    }

   
}
    

    export default (state=intialState,action)=>{
        let addQuantity="";
    switch(action.type){
        case ADD_PRODUCT_BASKET:
            addQuantity={...state.products[action.payload]};
            addQuantity.numbers+=1
            addQuantity.inCart=true
            console.log(addQuantity);
           
            return{
                ...state,
                basketNumbers:state.basketNumbers+1,
                cartCost:state.cartCost + state.products[action.payload].price,
                products:{
                    ...state.products,
                    [action.payload]:addQuantity
                }
    }

         case INCREASE_QUANTITY:
        addQuantity={...state.products[action.payload]}
        console.log(addQuantity)
       addQuantity.numbers+=1;
     
        return{
            ...state,
            cartCost:state.cartCost + state.products[action.payload].price,
          
            products:{
                ...state.products,
                [action.payload]:addQuantity
            }
           
             
        
}
case DECREASE_QUANTITY:
//     addQuantity={...state.products[action.name]}
//        console.log(addQuantity)
//    addQuantity.numbers-=1;
    return{
        ...state,
        // cartCost:state.cartCost + state.products[action.payload].price,
      
        // products:{
        //     ...state.products,
        //     [action.payload]:addQuantity
        // }
       
         
    
}
     
        default:
            return state;
    }
}