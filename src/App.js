import React, { Component } from 'react';
import Main from './Main/Main';
import  Navigation from './Header/Navigation';
import ProductDetails from './product/ProductDetails';
import Cart from './cart/Cart';
import Home from './Header/Home';


import {BrowserRouter as  Router, Route, Switch } from 'react-router-dom';


class App extends Component{
  constructor(props){
    super();
    this.state={
      cartItems:[]
    }
  }
  render(){
    return(
     
   <Router>
    <div className="App">
    <Navigation/>
     <Switch>
     <Route path='/' exact component={Home}/>
     <Route path="/products/:id" component={ProductDetails}/>
       <Route path="/cart" component={Cart}/>
     <Main/>
     
     </Switch>

    </div>
    </Router>
  
   
    
    )
  }
}
export default App;
